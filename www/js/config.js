/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Controllers
        'LoginCtrl':"js/controllers/LoginCtrl.js",
        'HomeCtrl':"js/controllers/HomeCtrl.js",
        'HomeCenterCtrl':"js/controllers/HomeCenterCtrl.js",
        'ChatsCtrl':"js/controllers/ChatsCtrl.js",
        'ChatDetailCtrl':"js/controllers/ChatDetailCtrl.js",
        'WorkCtrl':"js/controllers/WorkCtrl.js",
        'MeCtrl':"js/controllers/MeCtrl.js",
        
        'LHWelcomeCtrl':"js/controllers/lhctrl/LHWelcomeCtrl.js",
        'LHListCtrl':"js/controllers/lhctrl/LHListCtrl.js",
        'LHPublishCtrl':"js/controllers/lhctrl/LHPublishCtrl.js",
        'LHDetailCtrl':"js/controllers/lhctrl/LHDetailCtrl.js",
        
        'BGWelcomeCtrl':"js/controllers/bgctrl/BGWelcomeCtrl.js",
        'BGListCtrl':"js/controllers/bgctrl/BGListCtrl.js",
        'BGDetailCtrl':"js/controllers/bgctrl/BGDetailCtrl.js",
        'BGPublishCtrl':"js/controllers/bgctrl/BGPublishCtrl.js",
        
        'GJWelcomeCtrl':"js/controllers/tgctrl/GJWelcomeCtrl.js",
        'GJListCtrl':"js/controllers/tgctrl/GJListCtrl.js",
        'GJPublishCtrl':"js/controllers/tgctrl/GJPublishCtrl.js",
        'GJBiddingCtrl':"js/controllers/tgctrl/GJBiddingCtrl.js",
        'GJWarrantCtrl':"js/controllers/tgctrl/GJWarrantCtrl.js",
        
        'LWWelcomeCtrl':"js/controllers/lwctrl/LWWelcomeCtrl.js",
        'LWPublishCtrl':"js/controllers/lwctrl/LWPublishCtrl.js",
        
        
        //*** Services
        //'Utils':"js/services/Utils.js",
        'HomeService':"js/services/HomeService.js",
        'LHDataService':"js/services/LHDataService.js",
        'BGDataService':"js/services/BGDataService.js",
        'ChatService':"js/services/ChatsService.js",
        
        //****Directive
        'horizontalList':"js/directive/horizontalList.js",
        'timeCountDown':"js/directive/timeCountDown.js",
        
        //***  工具类
        /*'query':"lib/other/jquery-1.8.2.min.js",
        'jcountdown':"lib/other/jquery.jcountdown.min.js",*/
        
        //*** 主件
        //*** CSS
        'animateCSS':"css/animate.min.css"
    },
    
    //***申明组合
    ViewArgs: {
      	HomeArgs: ['HomeCtrl','HomeService',],
      	ChatsArgs: ['ChatsCtrl','ChatService'],
      	ChatDetailCtrlArgs: ['ChatDetailCtrl','ChatService'],
      	LHWelcomeCtrlArgs: ['LHWelcomeCtrl','animateCSS'],
      	LHCtrlArgs: ['LHListCtrl','LHDataService'],
      	LHPublishArgs: ['LHPublishCtrl','LHDataService'],
      	LHDetailCtrlArgs: ['LHDetailCtrl','LHDataService','timeCountDown'],
      	
      	BGWelcomeArgs: ['BGWelcomeCtrl','BGDataService','horizontalList'],
      	BGListArgs: ['BGListCtrl','BGDataService'],
      	BGDetailCtrlArgs: ['BGDetailCtrl','BGDataService'],
      	
      	LWWelcomeArgs : ['LWWelcomeCtrl','timeCountDown'],
      	LoginArgs: ['LoginCtrl']
    }
});
