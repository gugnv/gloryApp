/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('BGDetailCtrl', ['$scope', '$stateParams','$state','Utils', 'BGDataService', function($scope, $stateParams,$state, Utils,BGDataService) {
	//$scope.lists = LHDataService.alllists();
	//页面数据
	Utils.getUrlData('bgxinxi.txt').then(function(data){
        $scope.curruntpage = data[$stateParams.listId];
        $scope.curruntpage.othernum = parseInt($scope.curruntpage.zongnum)-parseInt($scope.curruntpage.buynum);
        $scope.curruntpage.otherbainum = (Math.round($scope.curruntpage.othernum/parseInt($scope.curruntpage.zongnum)*100))+"%";
    }).finally(function(){
    	if("1" == $scope.curruntpage.isbuy){
        	$scope.isbuyflag = {styleHide:{"display":"none"}};
        }else{
        	$scope.isbuyflag = {styleShow:{"display":"none"}};
        }
        teamLog($scope.isbuyflag);
    });
	
	//tab信息
	var tabsStyles = [{style1:{"border-bottom-style": "solid","color": "#FD8003"},
					   styleHide:{"display":"none"}
					  },{style2:{"border-bottom-style": "solid","color": "#FD8003"},
					   styleShow:{"display":"none"}
					 }]
	$scope.tabsStyle = tabsStyles[0];
	$scope.selecttabs = function(idx){
   		$scope.tabsStyle = tabsStyles[idx];
   	}
	
	//个人信息
	Utils.getUrlData('text.txt').then(function(data){
        $scope.person = data;
    }).finally(function(){});
	$scope.pingjiaArray = Utils.getStarArray(3);
	teamLog($scope.pingjiaArray);
	
	//评价信息
	Utils.getUrlData('evaluate.txt').then(function(data){
		var result = [];
        for(var i=0;i<data.length;i++){
        	var list = data[i];
        	list["starArray"] = Utils.getStarArray(list.evaluateLevel);
        	result.push(list);
        }
        $scope.evaluateArray = result;
    }).finally(function(){});
    
    
    $scope.goSubmit = function(){
   		$state.go('tab.bg-toBuy');
    }
    
    
    $scope.$on('$ionicView.afterEnter', function() {
		//BGDataService.init();
	});
}]);