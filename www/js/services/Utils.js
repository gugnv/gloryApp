//服务器请求地址
//var severUrl = "https://192.168.1.10:8080/qnh";
var severUrl = "http://192.168.1.10:8080/morningglory";
//http://192.168.1.10:8080/morningglory/SysUser/login

/**
 * 日志输出控制
 */
var isRelease = false;
//var isRelease = true;
function teamAlert(a){ if(!isRelease) alert(a); }
function teamLog(b){ if(!isRelease) console.log(b); }
/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.factory('Utils', function($q, $http,$ionicLoading) {
	
	var successText = '获取数据成功';
	var errorText = '获取数据失败,请检查网络';
	var durationTime = 1000;

	return {

		/**
		 * 显示Toast
		 * @param {Object} text
		 * @param {Object} duration
		 */
		showLoading:function(text,duration){
			//var opt = {template:text};
			var opt =   {
				           template: text,
				           content: 'Loading',
				           animation: 'fade-in',
				           showBackdrop: true,
				           maxWidth: 200,
				           position:'bottom',
				           showDelay: 0
				        };
			if(duration && duration!=0){
				opt.duration = duration;
			}
			$ionicLoading.show(opt);
		},
		/**
		 * 隐藏Toast
		 */
		hideLoading:function(){
			$ionicLoading.hide();
		},
		/**
		 * 用get方式获取数据
		 * @param {Object} url
		 */
		getUrlData:function(url,dotext,config){
			//teamLog(dotext);
			if (dotext != null) successText = dotext;
			teamLog(successText);
       		var deferred = $q.defer();
			$http.get(url,config)
	        .success(function (data) {
	            teamLog("getUrlData:"+JSON.stringify(data));
	            deferred.resolve(data);
			    toastShow(successText);
			}).error(function (data, status, headers, config) {
	            deferred.reject(data);
			    toastShow(errorText);
			});
	        
	        return deferred.promise;
		},
		/**
		 * 用Post方式获取数据
		 * @param {Object} url
		 * @param {Object} params
		 */
		postUrlData:function(url,params,dotext,config){
			if (dotext != null) successText = dotext;
			var deferred = $q.defer();
			$http.post(url,params,config)
	        .success(function (data) {
	            teamLog("postUrlData:"+JSON.stringify(data));
	            deferred.resolve(data);
			    toastShow(successText);
			}).error(function (data, status, headers, config) {
	            deferred.reject(data);
			  	toastShow(errorText);
			});
	        
	        return deferred.promise;
		},
		
		/**
		 * 用jsonp的方式获取数据
		 * @param {Object} method
		 * @param {Object} url
		 * @param {Object} params
		 * @param {Object} dotext
		 */
		jsonpUrlData:function(url,dotext,config){
			if (dotext != null) successText = dotext;
			var deferred = $q.defer();
			$http.jsonp(url,config)
	        .success(function (data) {
	            teamLog("jsonpUrlData:"+JSON.stringify(data));
	            deferred.resolve(data);
			    toastShow(successText);
			}).error(function (data, status, headers, config) {
	            deferred.reject(data);
	            toastShow(errorText);
			});
	        
	        return deferred.promise;
		},
		
		/**
		 * 根据评分值，获取评价打星图
		 * @param {Object} level
		 */
        getStarArray : function(level){
	   		var result = [];
	   		for(var i = 0; i < 5 ; i++) {
				teamLog(level);
				if(level >= i+1) {
					result.push("img/ghlh/star.png");
				} else {
					result.push("img/ghlh/starBack.png");
				}
			}
			return result;
	    }
		
	};
	/**
	 * 显示Toast
	 * @param {Object} doText
	 */
	function toastShow(doText){
		if(doText != ""){
			$ionicLoading.show({template: doText, noBackdrop: true, duration: 2000});
		}
	}
	
});

/*app.service('Constants',function(){
	//this.severUrl = "https://192.168.1.10:8080/qnh";
	return {
		severUrl:function(){
			return "https://192.168.1.10:8080/qnh";
		}
	};
	//return this;
			
});*/
