/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('LWWelcomeCtrl', ['$scope','$state','Utils',function($scope,$state,Utils) {
    var d = new Date().getTime()+1000000;
	$scope.endTime = new Date(d).toString();
	teamLog($scope.endTime);
	
	$scope.lists = [{
		idx:4,
		name:"万科A",
		price:1000,
		renqi:200,
		register:"张三"
	},{
		idx:3,
		name:"中车",
		price:300,
		renqi:500,
		register:"李四"
	},{
		idx:2,
		name:"茅台",
		price:1000,
		renqi:10,
		register:"康康"
	},{
		idx:1,
		name:"中国联通",
		price:500,
		renqi:100,
		register:"姗姗"
	}];
	
    $scope.isEnd = function(){
     	teamAlert("时间到了！");
    }
     
     $scope.rightClick = function(){
     	var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).addClass("tabs-item-hide");
     	$state.go("tab.lw-publish");
     }
     
     $scope.$on('$ionicView.afterEnter', function() {
        var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).removeClass("tabs-item-hide");
      
    });
    
    
}]);
