'use strict';
app.directive('scrollLeftList', function () {
    return {
        restrict: 'EA',
        transclude: true,
        template: '<div style="width: 100%;height: 100%;margin:0 auto;white-space: nowrap;overflow:hidden;">'
				+    '<div style="display: inline;" >'
			    +    '</div>'
				+    '<div style="display: inline;margin-left:-3px;" ng-transclude>'
			    +    '</div>'
			    
		        + '</div>',
        link: function (scope, ele, attrs) {
        	/*console.log(ele[0].childNodes[0].childNodes[0]);
        	console.log(ele[0].childNodes[0].childNodes[1]);*/
        	angular.element(ele[0]).ready(function(){
        		var speed = 20;
				var scroll_begin = ele[0].childNodes[0].childNodes[1];
				var scroll_end = ele[0].childNodes[0].childNodes[0];
				var scroll_div = ele[0].childNodes[0];
				//console.log(angular.element(scroll_begin).html());
				angular.element(scroll_end).append(angular.element(scroll_begin).html());
				scroll_div.scrollLeft = scroll_begin.offsetWidth + scroll_end.offsetWidth;
				//console.log(scroll_begin.offsetWidth + scroll_end.offsetWidth);
				//console.log(scroll_div.scrollLeft);
				var idxscrollleft = scroll_div.scrollLeft;
				function Marquee() {
					if(idxscrollleft - scroll_div.scrollLeft  >= scroll_begin.offsetWidth-3 ){
						scroll_div.scrollLeft += scroll_begin.offsetWidth;
					}else{
						scroll_div.scrollLeft--;
					}
				}
				var MyMar = setInterval(Marquee, speed);
				scroll_div.addEventListener('touchstart',function(){
					clearInterval(MyMar);
				},false);
				scroll_div.addEventListener('touchend',function(){
					MyMar = setInterval(Marquee, speed);
				},false);
        	});
        }
    }
});