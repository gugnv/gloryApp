'use strict';
/**
 * 倒计时控件
 */
app.directive('timeCountDown', function ($ionicLoading) {
    return {
        restrict: 'EA',
        replace: true,
        //transclude: true,
        //require: '^?horizontalList',//意思是将accordion的控制器传到指令中，从而在下方使用它的函数 ^的意思是需要遍历dom树，？的意思是找不到不报错       
        scope: true,
        templateUrl: 'page/util/countdown.html', 
        link: function (scope, ele, attrs) {
        	var timeEnd = function(diff){
        		if(diff > 100 * 24 * 60 * 60 * 1000) {
		    		$ionicLoading.show({template: "时间超过100天", noBackdrop: false, duration: 2000});
		    	}
		    	if(attrs.dayhide && diff > 3 *  24 * 60 * 60  * 1000) {
		    		$ionicLoading.show({template: "天数隐藏，时间超过3天", noBackdrop: false, duration: 2000});
		    	}
				if(attrs.hourhide && diff > 100 * 60 * 1000) {
					$ionicLoading.show({template: "小时隐藏，时间超过100分钟", noBackdrop: false, duration: 2000});
				}
				if(diff < 0) scope.$apply(attrs.timeend);
        	};
        	if(attrs.dayhide) angular.element(ele[0].childNodes[1].childNodes[1]).css({"display":"none"});
        	if(attrs.hourhide) angular.element(ele[0].childNodes[1].childNodes[3]).css({"display":"none"});
        	if(attrs.minutehide) angular.element(ele[0].childNodes[1].childNodes[5]).css({"display":"none"});
        	if(attrs.secondhide) angular.element(ele[0].childNodes[1].childNodes[7]).css({"display":"none"});
        	
        	if(attrs.labelhide){
        		angular.element(ele[0].childNodes[1].childNodes[1].childNodes[5]).css({"display":"none"});
        		angular.element(ele[0].childNodes[1].childNodes[3].childNodes[5]).css({"display":"none"});
        		angular.element(ele[0].childNodes[1].childNodes[5].childNodes[5]).css({"display":"none"});
        		angular.element(ele[0].childNodes[1].childNodes[7].childNodes[5]).css({"display":"none"});
        	}
        	if(attrs.marginright && attrs.marginright != ""){
        		var marginright = attrs.marginright+"px";
        		angular.element(ele[0].childNodes[1].childNodes[1]).css({"margin-right":marginright});
        		angular.element(ele[0].childNodes[1].childNodes[3]).css({"margin-right":marginright});
        		angular.element(ele[0].childNodes[1].childNodes[5]).css({"margin-right":marginright});
        	}
        	if(attrs.setsize && attrs.setsize != ""){
        		var width = attrs.setsize+"px" ,height = (1.28 * attrs.setsize)+"px";
        		angular.element(ele[0].childNodes[1].childNodes[1].childNodes[1].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[1].childNodes[3].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[3].childNodes[1].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[3].childNodes[3].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[5].childNodes[1].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[5].childNodes[3].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[7].childNodes[1].childNodes[1]).css({"width":width,"height":height});
        		angular.element(ele[0].childNodes[1].childNodes[7].childNodes[3].childNodes[1]).css({"width":width,"height":height});
        		if(!attrs.labelhide){
        			var labelwidth = (2.28 * attrs.setsize)+"px"
	        		angular.element(ele[0].childNodes[1].childNodes[1].childNodes[5]).css({"width":labelwidth});
	        		angular.element(ele[0].childNodes[1].childNodes[3].childNodes[5]).css({"width":labelwidth});
	        		angular.element(ele[0].childNodes[1].childNodes[5].childNodes[5]).css({"width":labelwidth});
	        		angular.element(ele[0].childNodes[1].childNodes[7].childNodes[5]).css({"width":labelwidth});
        		}
        	}
        	//teamLog(attrs.dayhide);
        	//teamLog(ele[0].childNodes[1].childNodes[1].childNodes[5]);
        	///angular.element(ele[0]).ready(function(){
        		countdown(scope,ele,attrs,timeEnd);
        	//});
        }
    };
    function getDateFromString(str){
    	if(str == null || str == "") return new Date();
    	return new Date(Date.parse(str.replace(/-/g, "/"))); 
    };
    
    function countdown(scope,ele,attrs,callback){
    	if(scope.endTime) attrs.enddate = scope.endTime;
    	var begindate = getDateFromString(attrs.begindate);
        var enddate = getDateFromString(attrs.enddate);
    	//teamLog(begindate);
    	//teamLog(enddate);
    	//teamLog(enddate - begindate);
    	if(enddate < begindate) return ;
    	var diff = (enddate - begindate) || 0;
    	if(diff >= 100 * 24 * 60 * 60 * 1000) {
    		if (callback) callback(diff);
			return;
    	}
    	if(attrs.dayhide && diff >= 3 *  24 * 60 * 60 * 1000) {
    		if (callback) callback(diff);
			return;
    	}
		if(attrs.hourhide && diff >= 100 * 60 * 1000) {
			if (callback) callback(diff);
			return;
		}
		diff = Math.floor( diff / 1000 );
		var oldnumb = 0;
		function update() {
			if (diff < 0) {
				if (callback) callback(diff);
				return;
			}
			formatDate(diff,ele,oldnumb,attrs);
			setTimeout(function() {
				oldnumb = diff;
				diff--;
				update();
			}, 1000);
		}
		update();
    };
    
    function formatDate(diff,ele,old,attrs){
    	
		var dayold = Math.floor(old / 86400);
		var hourold = Math.floor((old - dayold * 86400) / 3600);
		var minuteold = Math.floor((old - dayold * 86400 - hourold * 3600) / 60);
		var secondold = Math.floor(old - dayold * 86400 - hourold * 3600 - minuteold * 60);
    	//console.log("old时间  day："+dayold+"， hour："+hourold+"， minute："+minuteold+"， second："+secondold);
    	
    	var daynow = Math.floor(diff / 86400);
		var hournow = Math.floor((diff - daynow * 86400) / 3600);
		var minutenow = Math.floor((diff - daynow * 86400 - hournow * 3600) / 60);
		var secondnow = Math.floor(diff - daynow * 86400 - hournow * 3600 - minutenow * 60);
		//console.log("new时间  day："+daynow+"， hour："+hournow+"， minute："+minutenow+"， second："+secondnow);
		
		if(attrs.dayhide){
			hourold = hourold + (dayold * 24);
			hournow = hournow + (daynow * 24);
		}
		
		if(attrs.hourhide){
			minuteold += hourold * 60;
			minutenow += hourold * 60;
		}
		
    	if( secondold != secondnow ) moveDate(ele,secondnow,7,secondold);
    	if( minuteold != minutenow) moveDate(ele,minutenow,5,minuteold);
    	if( hourold != hournow) moveDate(ele,hournow,3,hourold);
    	if( dayold != daynow) moveDate(ele,daynow,1,dayold);
    };
    
    function moveDate(ele,numb,flag,idx){
    	var numb01 = Math.floor(numb/10), numb02 = Math.floor(numb%10),
			idx01 = Math.floor(idx/10),   idx02 = Math.floor(idx%10);
			
    	var child = ele[0].childNodes[1].childNodes[flag];
    	//teamLog(child);
    	if(numb01 != idx01){
    		var recls01 = "num"+numb01;
    		var addcls01 = "num"+idx01;
    		//teamLog(recls01 + "=11111=" + addcls01);
    		var child01 = child.childNodes[1].childNodes[1];
    		angular.element(child01).removeClass(addcls01).addClass(recls01);
    		//teamLog(child01);
    	}
    	
    	if(numb02 != idx02){
    		var recls02 = "num"+numb02;
	    	var addcls02 = "num"+idx02;
	    	//teamLog(recls02 + "=22222=" + addcls02);
	    	var child02 = child.childNodes[3].childNodes[1];
	    	angular.element(child02).removeClass(addcls02).addClass(recls02);
    		//teamLog(child02);
    	}
    }
});