/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('LHDetailCtrl', ['$scope', '$stateParams','Utils', 'LHDataService', function($scope, $stateParams, Utils,LHDataService) {
	//$scope.lists = LHDataService.alllists();
	Utils.getUrlData('text.txt').then(function(data){
        $scope.person = data;
    }).finally(function(){});
	$scope.curruntpage = LHDataService.getlist($stateParams.listId);
	Utils.getUrlData('evaluate.txt').then(function(data){
		var result = [];
        for(var i=0;i<data.length;i++){
        	var list = data[i];
        	list["starArray"] = Utils.getStarArray(list.evaluateLevel);
        	result.push(list);
        }
        $scope.evaluateArray = result;
    }).finally(function(){});
    $scope.goSubmit = function(){
   		teamAlert(111);
    }
   
}]);