/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.factory('Drag',function() {
	//窗体宽度的一半
	var heftwidth = (document.body.clientWidth)/2;
	//var screenheigth = document.body.clientHeight;
	//teamLog(screenheigth);
	//60度值
	var disangle = Math.PI / 3;
	//圆心值
	var oo = {
		x: heftwidth,
		y: heftwidth
	};
	//初始数据
	var initPosition = [{
		id: "bgid",
		title: "包裹集市",
		text: "中国股市上唯一长期有效的方法可<br/>能就是掌握信息了，这是一个自由的<br/>信息交易市场，牛人可将荐股信息打包出<br/>售，并对限期收益率进行对赌，你会发现，自<br/>从利益捆绑之后，股票推荐变得准确多了......",
		angle: 5 * disangle
	}, {
		id: "lhid",
		title: "股海领航",
		text: "不参加实战是学不会投资的，所以<br/>这是一个实战型的在线学习平台，牛<br/>人们可以在本平台上买船创业，乘客们只<br/>需支付一点每日票价就能得到高人指点了，更<br/>重要的是，你还可以随时弃船换乘，于是在<br/>残酷竞争之下，船长们各显身手......",
		angle: 4 * disangle
	}, {
		id: "qnid",
		title: "牵牛宝",
		text: "与持有基金管理牌照的正规机构合<br/>作，由包裹集市、挑管家以及股海领<br/>航三个版块的每月收益率前三名负责操盘，<br/>跑马不相马，所有人均可参与投资，以每日净<br/>值作价，聚沙成塔，风险自负，从此牛人开始帮<br/>你理财......",
		angle: 3 * disangle
	}, {
		id: "hhid",
		title: "后花园",
		text: "茶余饭后，忙里偷闲，不妨参与一<br/>些确实公正无欺的游戏，爱玩之人汇<br/>集起来，这是一个开心的乐园......",
		angle: 2 * disangle
	}, {
		id: "tgid",
		title: "挑管家",
		text: "长期以来，帐户委托操盘都是发生<br/>在熟人之间，分利高达两三成，在这<br/>里只需挂出百分之一以上的利润分成，就<br/>会有天下英雄来应，就看你慧眼识英才了，<br/>牛人们更可以在家办公，一个人看管数十<br/>个帐户......",
		angle: 1 * disangle
	}, {
		id: "lwid",
		title: "涨停列车",
		text: "任何一只股票的涨停，都意味着在<br/>中国股市上的无限动能，所有的参与<br/>者都可以在非交易时段把对涨停股的判断<br/>投入到不同级别的股票池中，收盘后涨停者<br/>可以分走池内所有的金额，以奖励他非凡<br/>的判断力......",
		angle: 0 * disangle
	}];
	
	var is_drag = false,//是否转动中
		is_control = false,
	    temp_timer_1 = 0,//惯性开始时间
		temp_timer_2 = 0,//惯性结束时间
		is_interval ;
	
	//返回的函数集
	return {
		initdata:function(){
			lastInit(); 
		},
		cotrInitData : function (){
			drag();
		},
		isDrag : function (){
			//teamLog(is_control);
			return is_control;
		}
	};
	
	function getControlFlag(){
		if(is_drag){
			is_drag = false;
			return false;
		}else{
			return true;
		}
	}

	/**
	 * 设置中心图片和六个模块的位置
	 */
	function setPosition(posid,posangle){
		//连带转动中心图片的位置
		var objpos = document.getElementById(posid);
		if("hhid" == posid) {
			var centerpos = document.getElementById("homeoo");
			centerpos.style.transform = "rotateZ(" + ( posangle * 180 / Math.PI) + "deg)";
			centerpos = null;
		}
		/*objpos.style.top = ( heftwidth * 0.6 - heftwidth * 0.12 + heftwidth * (110 / 155) * Math.cos(posangle)) + "px";
		objpos.style.left = ( heftwidth - heftwidth * 0.24 + heftwidth * (110 / 155) * Math.sin(posangle)) + "px";*/
		objpos.style.top = ( heftwidth - (0.18*heftwidth) + heftwidth * (105 / 155) * Math.cos(posangle)) + "px";
		objpos.style.left = ( heftwidth - (0.18*heftwidth) + heftwidth * (105 / 155) * Math.sin(posangle)) + "px";
		objpos = null;
	}
	
	//结束时，定位六个模块的位置
	function lastInit() {
		var result = [];
		for(var i = 0; i < initPosition.length; i++) {
			var list = initPosition[i];
			//var lastAngle = Math.floor(initPosition[i].angle / disangle)*disangle;
			//var lastAngle = Math.ceil(initPosition[i].angle / disangle)*disangle;
			var lastAngle = Math.round(initPosition[i].angle / disangle)*disangle;

			if(lastAngle % (6 * disangle) == 0 ){
				document.getElementById("texttitle").innerHTML = initPosition[i].title;
				document.getElementById("textid").innerHTML = initPosition[i].text;
			}
			list.angle = lastAngle;
			result.push(list);
			setPosition(initPosition[i].id,lastAngle);
		}
		initPosition = result;
		is_interval = null;
		is_control = getControlFlag();
	}
	
	/**
	 * 转动事件主体
	 * @param obj:被拖动的区域
	 */
	function drag() {
		
	    var obj = document.querySelector('#parentid')
	    var downX,downY,moveX,moveY;
		var cur_angle = 0; 
		obj.addEventListener('touchstart',function(ev){
			//teamLog("touchstart");
			if(is_drag){
				clearInterval(is_interval);
				lastInit();
			}
			temp_timer_1 = temp_timer_2 = new Date().getTime();
			var oEvent = ev || event;
			downX = oEvent.touches[0].clientX - oo.x;
			downY = oEvent.touches[0].clientY - oo.y;
			cur_angle = 0;
		},false);
		
		obj.addEventListener('touchmove',function(ev){
			ev.preventDefault();
			//teamLog("touchmove");
			var oEvent = ev || event;
	
			moveX = oEvent.touches[0].clientX - oo.x;
			moveY = oEvent.touches[0].clientY - oo.y;
	
			var angle = getAngleFromPoint(downX, downY, moveX, moveY);
			temp_timer_1 = temp_timer_2;
			temp_timer_2 = new Date().getTime();
	
			if(checkGoTo(downX, downY, moveX, moveY)) {
				cur_angle = angle;
				initPosition = resetInitData(angle, 1);
			} else {
				cur_angle = 0 - angle;
				initPosition = resetInitData(angle, -1);
			}
			downX = moveX;
			downY = moveY;
		},false);
		
		obj.addEventListener('touchend',function(ev){
			//teamLog("touchend");
			inertiaShow(cur_angle);
		},false);
		
		
	}
	/**
	 * 监测往哪个方向转动
	 * @param {Object} doX
	 * @param {Object} doY
	 * @param {Object} moX
	 * @param {Object} moY
	 * return false 逆时针
	 */
	function checkGoTo(doX, doY, moX, moY) {
		var result = false;
		if( moX>0 && moY > doY ){
			result = true;
		}
		if( moX<0 && moY < doY ){
			result = true;
		}
		if(moY>0 && moX < doX ){
			result = true;
		}
		if(moY<0 && moX > doX ){
			result = true;
		}
		return result;
	}
	/**
	 * 计算移动的两点与圆心之间的角度（弧度）
	 * @param {Object} doX
	 * @param {Object} doY
	 * @param {Object} moX
	 * @param {Object} moY
	 */
	function getAngleFromPoint(doX, doY, moX, moY) {
		//teamLog("doX:"+doX+"   doY:"+doY+"   moX:"+moX+"   moY:"+moY);
		var a2 = Math.pow((doX - moX), 2) + Math.pow((doY - moY), 2);
		var b2 = Math.pow((moX), 2) + Math.pow((moY), 2);
		var c2 = Math.pow((doX), 2) + Math.pow((doY), 2);
		//teamLog("a2:" + a2 + "  b2:" + b2 + "  c2:" + c2);
		var b2c = 2 * Math.sqrt(b2) * Math.sqrt(c2);
		var cosAngle = 1;
		if(b2c != 0) {
			cosAngle = (b2 + c2 - a2) / b2c;
		}
		return Math.acos(cosAngle);
	}
	
	/**
	 * 转动后重置六个模块位置的初始化值
	 * @param {Object} ang2
	 * @param {Object} flag
	 */
	function resetInitData(ang2, flag) {
		var result = [];
		for(var i = 0; i < initPosition.length; i++) {
			var list = initPosition[i];
			var ang1 = initPosition[i].angle - (flag * ang2);
			var ang3 = ang1;
			if(ang1 > 2 * Math.PI || ang1 == 2 * Math.PI) {
				ang1 = ang1 - 2 * Math.PI;
			} 
			if(ang1 < -2 * Math.PI || ang1 == -2 * Math.PI){
				ang1 = ang1 + 2 * Math.PI;
			}
			/*if(ang3 != ang1){
				teamLog(("ang3="+ang3+" ; ang1="+ang1+" ; angd="+(ang3-ang1)/Math.PI));
			}*/
			list.angle = ang1;
			result.push(list);
			setPosition(list.id,ang1);
		}
		//teamLog(JSON.stringify(result));
		return result;
	}
	
	/**
	 * 旋转惯性
	 * @param {Object} tangle
	 */
	function inertiaShow(tangle) {
		//teamLog(tangle);
		var speed = 0.00005;
		var avr = 0;
		if(temp_timer_2 != temp_timer_1) {
			avr = ((3 * tangle) / (temp_timer_2 - temp_timer_1));
		}
		if(Math.abs(avr)<0.02){
			avr = 0;
		}
		if(avr > 0.075){
			avr = 0.075;
		}
		if(avr < -0.075){
			avr = -0.075;
		}
		//teamLog(avr);
		if(avr > 0) {
			is_interval = setInterval(function() {
				is_drag = true;
				avr -= speed;
				initPosition = resetInitData(avr, 1);
				if(avr < 0 || avr == 0) {
					clearInterval(is_interval);
					setTimeout(function(){lastInit();is_drag = false;},100);
				}
			}, 5);
		} else if(avr < 0) {
			is_interval = setInterval(function() {
				is_drag = true;
				avr += speed;
				initPosition = resetInitData(-1*avr, -1);
				if(avr > 0 || avr == 0) {
					clearInterval(is_interval);
					setTimeout(function(){lastInit();is_drag = false;},100);
				}
			}, 5);
		}else{
			setTimeout(function(){lastInit();},100);
		}
	}
});
