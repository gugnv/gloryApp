'use strict';
/**
 * 倒计时控件
 */
app.directive('wavesClick', function () {
    return {
        restrict: 'A',
        link: function postLink(scope, ele, attrs) {
        	ele[0].style.position = 'relative';
        	angular.element(ele[0]).ready(function(){
        		var containers = Array.prototype.slice.call(ele);
			    for (var i = 0; i < containers.length; i++) {
			        var canvas = document.createElement('canvas');
			        canvas.addEventListener('click', function(event){
			        	var color = attrs.wavesClick;
				      	var element = event.toElement;
				      	var context = element.getContext('2d');
				      	var radius = 0,t = 0;
				      	var centerX = event.offsetX;
				      	var centerY = event.offsetY;
				      	//draw();
				      	var timer = setInterval(function(){
				      	  	context.beginPath();
					      	//context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
					      	context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
					      	context.fillStyle = color;
					      	context.fill();
					      	t = t + 0.05;
					      	radius += (2 + (t*t));
					      	if(radius > element.width){
					      		context.clearRect(0, 0, element.width, element.height);
					      		clearInterval(timer);
					      	}
				      	},10);
			        }, false);
			        containers[i].appendChild(canvas);
			        canvas.style.width ='100%';
			        canvas.style.height='100%';
			        canvas.style.opacity = '0.25';
			        canvas.style.position = 'absolute';
			        canvas.style.top = '0';
			        canvas.style.left = '0';
			        canvas.width  = canvas.offsetWidth;
			        canvas.height = canvas.offsetHeight;
			    }
        	});
        }
    };
});