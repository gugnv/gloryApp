'use strict';
app.directive('upMarquee', function () {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        template: '<div style="" ng-transclude></div>',
        link: function (scope, ele, attrs) {
        	var i, height = parseInt(attrs.nodeHeigth);
        	var numb = parseInt(attrs.showCol);
        	angular.element(ele[0].parentElement).css({"height":height*numb+"px","overflow":"hidden"});
        	angular.element(ele[0]).ready(function(){
        		for(i=0;i<angular.element(ele[0]).children().length;i++){
	        		angular.element(angular.element(ele[0]).children()[i]).css({"height":height+"px","line-height":height+"px","margin":0});
	        	}
	        	var marquee = ele[0];
	        	var scrollheight =marquee.offsetHeight;
	            var offset=0;
	            for(i=0;i<numb;i++){
	            	var childNode = marquee.children[i].cloneNode(true);
	            	marquee.appendChild(childNode);//还有这里
	            }
	            setInterval(function(){
	                if(offset == scrollheight){
	                    offset = 0;
	                }
	                marquee.style.marginTop = "-"+offset+"px";
	                offset += 1;
	            },100);
        	});
        	
        }
    };
});