/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('HomeCtrl', ['$scope','$state','Drag','$ionicModal', function($scope,$state,Drag,$ionicModal) {
    Drag.initdata();
	Drag.cotrInitData();
	$scope.goPage = function(strid){
		if(Drag.isDrag()){
			$state.go(strid);
		}
	}
	
	var shicis = [{
		"name":"岭上白云舒复卷<br/>天边皓月去还来<br/>低头却入茅檐下<br/>不觉呵呵笑几回"},{
		"name":"岁月人间促<br/>烟霞此地多<br/>殷勤竹林寺<br/>能得几回过"},{
		"name":"众星罗列夜明珠<br/>岩点孤灯月未治<br/>圆满光华不磨莹<br/>挂在青天是我心"},{
		"name":"城外土馒头<br/>馅草在城里<br/>一人吃一个<br/>莫嫌没滋味"},{
		"name":"步步穿篱入境幽<br/>松高柏老几人游<br/>花开花落非僧事<br/>自有清风对碧流"},{
		"name":"尽日寻春不见春<br/>芒鞋踏遍陇头云<br/>归来笑拈梅花嗅<br/>春在枝头已十分"},{
		"name":"为爱寻光纸上钻<br/>不能透处几多般<br/>忽然撞着来时路<br/>始觉半生被眼瞒"},{
		"name":"君不见<br/>三界之中纷扰<br/>只为天明不了绝<br/>一念不生心澄然<br/>无去无来不生灭"},{
		"name":"自笑老夫筯力败<br/>偏恋松岩爱独游<br/>可叹往年至今日<br/>任运还同不系舟"},{ 
		"name":"南台静坐一炉香<br/>终日凝然万虑忘<br/>不是息心去妄想<br/>都缘无事可商量"},{ 
		
		"name":"六祖当年不丈夫<br/>倩人书壁自糊涂<br/>分明有偈言无信<br/>却受他家一钵盂"},{
		"name":"山县萧条早放衙<br/>莲塘无主自开花<br/>三叉路口炊烟起<br/>自瓦青旗一二家"},{
		"name":"他人骑大马<br/>我独骑驴子<br/>回顾担柴汉<br/>心下较些子"},{ 
		"name":"一树春风有两般<br/>南枝身暧北枝寒<br/>现前一段西来意<br/>一片西飞一片东"},{ 
		"name":"祖师遗下一只履<br/>千古万古播人耳<br/>空自肩担跣足行<br/>何曾踏着自家底"},{
		"name":"瞋是心中火<br/>能烧功德林<br/>欲行菩萨道<br/>忍辱护真心"},{
		"name":"独坐清谈久亦劳<br/>碧松燃火暖衾袍<br/>夜深童子唤不起<br/>猛虎一声山月高"},{
		"name":"碧涧泉水清<br/>寒山月华白<br/>默知神自明<br/>观空境逾寂"},{
		"name":"苍苍竹林寺<br/>杳杳钟声晚<br/>荷笠带斜阳<br/>青山独归远"},{
		"name":"眼入毫端写竹真<br/>枝掀叶举见精神<br/>因知幻物出天象<br/>问取人间老斫轮"

	}];

	$ionicModal.fromTemplateUrl('page/util/popup.html', {
	    scope: $scope,
	    animation: 'slide-in-upslow'
	    //animation: 'slide-in-left'
	    //animation: 'slide-in-right'
	}).then(function(modal) {
	    $scope.modal = modal;
	});
	$scope.openModal = function() {
		var idx = Math.floor(Math.random()*20);
		$scope.shiciarray = (shicis[idx].name).split('<br/>');
		/*console.log($scope.shiciarray);
		console.log(idx);*/
	    $scope.modal.show();
	};
	$scope.closeModal = function() {
	    $scope.modal.hide();
	};
	//当我们用完模型时，清除它！
	$scope.$on('$destroy', function() {
	    $scope.modal.remove();
	});
	// 当隐藏模型时执行动作
	$scope.$on('modal.hide', function() {
	    // 执行动作
	});
	// 当移动模型时执行动作
	$scope.$on('modal.removed', function() {
	    // 执行动作
	});

}]);
