/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('GJBiddingCtrl', ['$scope','$stateParams','$state',function($scope,$stateParams,$state) {
	$scope.introduces = [{
		"id" : "10",
		"imgUrl":"img/max.png",
		"name":"张三",
		"description":"如果无法简洁的表达您的想法，只能说明你还不够了解它",
		"date":"2017/01/03",
		"support" : "45",
		"criticism" : "23"
	},{
		"id" : "11",
		"imgUrl":"img/mike.png",
		"name":"李四",
		"description":"如果无法简洁的表达您的想法，只能说明你还不够了解它,如果无法简洁的表达您的想法，只能说明你还不够了解它,如果无法简洁的表达您的想法，只能说明你还不够了解它",
		"date":"2017/01/03",
		"support" : "45",
		"criticism" : "23"
	}];
	$scope.doSubmit = function(idx){
		$state.go("tab.tg-warrant");
	}
}]);
