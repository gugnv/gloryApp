'use strict';
/**
 * 键盘弹出，隐藏tab或者底部含class为bottom-btn的button
 */
app.directive('keyboardHandler', function ($window) {
    return {
        restrict: 'A',
        link: function postLink(scope, ele, attrs) {
        	var ishidetab = false;
            angular.element($window).bind('native.keyboardshow', function() {
				var fixedbtns = document.getElementsByClassName('bottom-btn');
            	if(fixedbtns.length!=0){
            		for(var i=0;i< fixedbtns.length;i++){
                		angular.element(fixedbtns[i]).addClass("myhide");
	                }
            	}else{
            		if(angular.element(ele).hasClass("tabs-item-hide")){
            			ishidetab = true;
            		}else{
            			ishidetab = false;
            			angular.element(ele).addClass("tabs-item-hide");
            		}
            	}
                var objconts = document.getElementsByTagName('ion-content');
                
                for(var i=0;i< objconts.length;i++){
                	angular.element(objconts[i]).css('bottom', '0px');
                }
				//angular.element(objconts[objconts.length-1]).css('bottom', 0);
            });

            angular.element($window).bind('native.keyboardhide', function() {
            	var fixedbtns = document.getElementsByClassName('bottom-btn');
            	if(fixedbtns.length!=0){
            		for(var i=0;i< fixedbtns.length;i++){
                		angular.element(fixedbtns[i]).removeClass("myhide");
	                }
            	}else{
            		if(!ishidetab){
            			angular.element(ele).removeClass("tabs-item-hide");
            		}
            	}
                var objconts = document.getElementsByTagName('ion-content');
                for(var i=0;i< objconts.length;i++){
                	angular.element(objconts[i]).css('bottom', '47px');
                }
            });
        }
    };
}).directive('clickAnimate', function () {
	/**
	 * 点击按钮，行动话效果
	 */
    return {
        restrict: 'A',
        scope:true,
        link: function postLink(scope, ele, attrs) {
        	//teamLog(scope);
        	var animateClass = attrs.clickAnimate +' animated infinite';
        	var $animate = angular.element(ele[0]),timer = null,downY,moveY,isScroll = false;
        	ele[0].addEventListener('touchstart',function(ev){
        		//teamLog("touchstart");
        		isScroll=false;
        		if(timer) clearTimeout(timer);
				$animate.removeClass(animateClass).addClass(animateClass);
				downY = ev.touches[0].clientY;
        	},false);
        	ele[0].addEventListener('touchmove',function(ev){
        		//teamLog("touchstart");
        		ev.preventDefault();
        		moveY = ev.touches[0].clientY;
        		if(moveY != downY) {$animate.removeClass(animateClass);isScroll=true; return;}
        	},false);
        	ele[0].addEventListener('touchend',function(ev){
        		//teamLog("touchstart");
        		if(!isScroll){
        			timer = setTimeout(function(){
						$animate.removeClass(animateClass);
						scope.$apply(attrs.doclick);
					}, 500);
        		}
        	},false);
        }
    };
}).directive('tgjClickAnimate', function () {
	/**
	 * 挑管家电话按钮效果
	 */
    return {
        restrict: 'A',
        scope:true,
        link: function postLink(scope, ele, attrs) {
        	teamLog(scope);
        	var obj = ele[0];
        	var $animate = angular.element(obj),downX,moveX,downY,moveY;
        	var parentobj = obj.parentElement;
        	/*$animate.ready(function() {
        		$animate.css({"position": "absolute"});
		        obj.style.left = (parentobj.offsetWidth-obj.offsetWidth)/2 + 'px';
		    });*/
        	obj.addEventListener('touchstart',function(ev){
        		//teamLog("touchstart");
				var oEvent = ev || event;
				downX = oEvent.touches[0].clientX;
				downY = oEvent.touches[0].clientY;
        	},false);
        	obj.addEventListener('touchmove',function(ev){
        		//teamLog("touchstart");
        		var oEvent = ev || event;
				moveX = oEvent.touches[0].clientX;
				moveY = oEvent.touches[0].clientY;
				if(Math.abs(moveY-downY) < Math.abs(moveX-downX)){
					ev.preventDefault();
					ev.stopPropagation();
					if(obj.offsetLeft>=30 && obj.offsetLeft <= parentobj.offsetWidth-obj.offsetWidth-30){
						$animate.css({"position": "absolute"});
						obj.style.left = (moveX - downX + obj.offsetLeft) + 'px';
					}
				}else{
					return;
				}
				downX = moveX;
        	},false);
        	obj.addEventListener('touchend',function(ev){
        		//teamLog("touchstart");
        		if(obj.offsetLeft <= parentobj.offsetWidth*0.8/3 ){
        			//obj.style.left = 30 + 'px';
        			scope.$apply(attrs.leftclick);
        		}
        		if(obj.offsetLeft >= parentobj.offsetWidth*1.8/3){
        			//obj.style.left = parentobj.offsetWidth-obj.offsetWidth-30 + 'px';
        			scope.$apply(attrs.rightclick);
        		}
        		//if(parentobj.offsetWidth*0.8/3 < obj.offsetLeft && obj.offsetLeft < parentobj.offsetWidth*1.8/3) 
        		//setTimeout(function(){
        			obj.style.left = (parentobj.offsetWidth-obj.offsetWidth)/2 + 'px';
        		//},200);
        	},false);
        }
    };
}).directive('ztlcClickAnimate', function () {
	/**
	 * 挑管家电话按钮效果
	 */
    return {
        restrict: 'A',
        scope:true,
        link: function postLink(scope, ele, attrs) {
        	var obj = ele[0];
        	var $animate = angular.element(obj),downX,moveX,downY,moveY;
        	var parentobj = obj.parentElement;
        	obj.addEventListener('touchstart',function(ev){
        		//teamLog("touchstart");
				var oEvent = ev || event;
				downX = oEvent.touches[0].clientX;
				downY = oEvent.touches[0].clientY;
        	},false);
        	obj.addEventListener('touchmove',function(ev){
        		var oEvent = ev || event;
				moveX = oEvent.touches[0].clientX;
				moveY = oEvent.touches[0].clientY;
				if(Math.abs(moveY-downY) < Math.abs(moveX-downX)){
					ev.preventDefault();
					ev.stopPropagation();
					if(obj.offsetLeft>=0 && obj.offsetLeft <= parentobj.offsetWidth-obj.offsetWidth-6){
						$animate.css({"position": "absolute"});
						obj.style.left = (moveX - downX + (obj.offsetLeft-6)) + 'px';
					}
				}else{
					return;
				}
				downX = moveX;
        	},false);
        	obj.addEventListener('touchend',function(ev){
        		if(obj.offsetLeft >= parentobj.offsetWidth*1.8/3){
        			scope.$apply(attrs.rightclick);
        		}
        		obj.style.left = '0px';
        	},false);
        }
    };
});