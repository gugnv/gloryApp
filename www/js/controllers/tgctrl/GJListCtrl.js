/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('GJListCtrl', ['$scope','$state','Utils',function($scope,$state,Utils) {
		//tab信息
	var tabsStyles = [{style1:{"border-bottom-style": "solid","color": "#FD8003"},
					   styleHide:{"display":"none"}
					  },{style2:{"border-bottom-style": "solid","color": "#FD8003"},
					   styleShow:{"display":"none"}
					 }]
	$scope.tabsStyle = tabsStyles[0];
	$scope.selecttabs = function(idx){
   		$scope.tabsStyle = tabsStyles[idx];
   	}
	
	$scope.gotonext = function(idx){
		$state.go("tab.tg-detail",{listId:idx});
	};
	
	$scope.lists = [{
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}, {
		renqi: 123,
		price: 234,
		shouyi: 432
	}];
}]);
