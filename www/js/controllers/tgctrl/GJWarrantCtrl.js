/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('GJWarrantCtrl', ['$scope','$stateParams','$state',function($scope,$stateParams,$state) {
	var introduces = [{
		"id" : "10",
		"imgUrl":"img/max.png",
		"name":"张三",
		"description":"如果无法简洁的表达您的想法，只能说明你还不够了解它",
		"date":"2017/01/03",
		"support" : "45",
		"criticism" : "23"
	},{
		"id" : "11",
		"imgUrl":"img/mike.png",
		"name":"李四",
		"description":"如果无法简洁的表达您的想法，只能说明你还不够了解它,如果无法简洁的表达您的想法，只能说明你还不够了解它,如果无法简洁的表达您的想法，只能说明你还不够了解它",
		"date":"2017/01/03",
		"support" : "45",
		"criticism" : "23"
	}];
	$scope.introduce = introduces[1];
	$scope.doSubmit = function(){
		
	}
}]);
