'use strict';
app.directive('horizontalList', function () {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        //require: '^?horizontalList',//意思是将accordion的控制器传到指令中，从而在下方使用它的函数 ^的意思是需要遍历dom树，？的意思是找不到不报错       
        /*scope: {
            someid: '=id'
        },*/
        template: '<div style="width: 100%;height: 100%;margin:0;position: relative;text-align: left;overflow: hidden;white-space: nowrap;">'
				+    '<div style="display: block;height: 100%;width: auto;position: absolute;" ng-transclude>'
			    +    '</div>'
		        + '</div>',
        link: function (scope, ele, attrs) {
        	var obj = ele[0].childNodes[0];
        	var parentobj = ele[0];
		    var iSpeedX=0,downX,moveX,downY,moveY,timer = null;
			obj.addEventListener('touchstart',function(ev){
				//teamLog("touchstart");
				//clearInterval(timer);
				var oEvent = ev || event;
				downX = oEvent.touches[0].clientX;
				downY = oEvent.touches[0].clientY;
			},false);
			
			obj.addEventListener('touchmove',function(ev){
				//teamLog("touchmove");
				
				var oEvent = ev || event;
				moveX = oEvent.touches[0].clientX;
				moveY = oEvent.touches[0].clientY;
				//teamLog(moveY-downY)
				if(Math.abs(moveY-downY) < Math.abs(moveX-downX)){
					ev.preventDefault();
					ev.stopPropagation();
					iSpeedX = moveX - downX;
					if(obj.offsetLeft<=0 && obj.offsetLeft >= parentobj.offsetWidth-obj.offsetWidth){
						obj.style.left = (iSpeedX + obj.offsetLeft) + 'px';
					}
				}else{
					return;
				}
				downX = moveX;
				downY = moveY;
				//teamLog(widthParent-obj.offsetWidth);
				//teamLog(obj.offsetLeft);
				//teamLog(slideLeft);
			},false);
			
			obj.addEventListener('touchend',function(ev){
				//teamLog("touchend");
				//移动函数，主要操作是计算鼠标移动速度和移动方向。
				clearInterval(timer);
				//iSpeedX = iSpeedX;
				//teamLog(iSpeedX);
				if(iSpeedX > 70) iSpeedX = 70;
				if(iSpeedX < -70) iSpeedX = -70;
				var speed = 2;
				timer = setInterval(function() {
					if(iSpeedX > 0){
						iSpeedX -= speed;
					}else if(iSpeedX < 0){
						iSpeedX += speed;
					}else{
						
					}
					var l = obj.offsetLeft + iSpeedX;
					if(l < parentobj.offsetWidth - obj.offsetWidth) {
						l = parentobj.offsetWidth - obj.offsetWidth;
						//iSpeedX *= -0.2;
						iSpeedX = 0;
					}
					if(l > 0) {
						l = 0;
						//iSpeedX *= -0.2;
						iSpeedX = 0;
					}
	
					obj.style.left = l + 'px';
	
					if(Math.abs(iSpeedX) < 2) iSpeedX = 0;
					if(iSpeedX == 0) {
						clearInterval(timer);
					}
					//teamLog(l);
					//teamLog(iSpeedX);
				}, 30);
			},false);
        }
    }
});