/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('ChatsCtrl', ['$scope', 'Chats',function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
  
}]);
