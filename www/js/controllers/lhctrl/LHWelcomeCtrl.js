/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('LHWelcomeCtrl', ['$scope','$state','Utils',function($scope,$state,Utils) {
	$scope.lists = [{
		url:"img/ghlh/lhlist01.png"
	},{
		url:"img/ghlh/lhlist02.png"
	},{
		url:"img/ghlh/lhlist03.png"
	}];
	$scope.goclick = function (idx){
		if(idx==0){
			var tabs =document.getElementsByTagName('ion-tabs');
        	angular.element(tabs).addClass("tabs-item-hide");
			$state.go("tab.lh-publish");
		}else{
			$state.go("tab.lh-list");
		}
	}
	$scope.$on('$ionicView.afterEnter', function() {
        var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).removeClass("tabs-item-hide");
    });
}]);
