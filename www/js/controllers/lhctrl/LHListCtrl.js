/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('LHListCtrl', ['$scope','$state','Utils','LHDataService',function($scope,$state,Utils,LHDataService) {
	//LHDataService.testshow()
	$scope.noMorePage = false;
	$scope.lists = LHDataService.alllists();
	$scope.gotonext = function(idx){
		var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).addClass("tabs-item-hide");
		$state.go("tab.lh-detail",{listId:idx});
	};

	$scope.doRefresh = function() {
        Utils.getUrlData('LHlists.txt').then(function(data){
        	$scope.noMorePage = false;
	        $scope.lists = LHDataService.alllists();
	        //console.log(LHDataService.alllists());
	    }).finally(function(){
	    	$scope.$broadcast('scroll.refreshComplete');
	    });
		
	};
    $scope.loadMore = function() {
        Utils.getUrlData('LHlists.txt',"").then(function(data){
	        if(data == null || data.length < 10){
	        	$scope.noMorePage=true;
	        }
	        for (var i=0;i<data.length;i++){
                $scope.lists.push(data[i]);
            }
	    }).finally(function(){
    		$scope.$broadcast('scroll.infiniteScrollComplete');
	    });
    };
    $scope.$on('$ionicView.afterEnter', function() {
    	teamLog("0"+location.hash);
        var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).removeClass("tabs-item-hide");
    });
}]);
