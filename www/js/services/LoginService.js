/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.factory('LoginService', function($q,$http,$state,$ionicLoading) {
	return {
		logon:logon,
		register:register,
		logout:logout,
		islive:islive
	};
	function logon(username,password){
		//console.log(username+"==="+password)
		var deferred = $q.defer();
        //var url = severUrl + '/SysUser/login';
        var url = 'userinfo.txt';
        //$http.post(url,{'username':username,"password":password})
        $http.get(url)
        .success(function (data) {
        	teamLog("logon:"+JSON.stringify(data));
        	//teamLog(data[0]);
		    //业务处理
		    //if(data[0].code=="OK"){
		    	localStorage.haslogin = 0;
            	//localStorage.ticken = data[0].message;
            	localStorage.ticken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjMxMjMiLCJyb2xlSWRzIjoiIiwicm9sZXMiOiIiLCJpc3MiOiJxaWFubml1a2VqaSIsImlhdCI6MTQ4Njk1NTA3OSwiZXhwIjoxNTAyNTA3MDc5LCJuYmYiOjE0ODY5NTUwNzl9.dbwjGG4S1o_b9S33h-nmRxrXW8YsLNwwBGmWvteof-A";
            	deferred.resolve(data);
		   	 	$ionicLoading.show({template: "登录成功", noBackdrop: true, duration: 1000});
		   /* }else{
		    	$ionicLoading.show({template: "登录失败,请稍后再试！", noBackdrop: true, duration: 1000});
		    }*/
		}).error(function (data, status, headers, config) {
		  	//业务处理
            deferred.reject(data);
		  	$ionicLoading.show({template: "登录失败,请稍后再试！", noBackdrop: true, duration: 1000});
		});
        
        return deferred.promise;
	};
	function register(username,password){
		var deferred = $q.defer();
        var url = 'userinfo.txt';
        $http.get(url)
        .success(function (data) {
		    //业务处理
            localStorage.haslogin = 0;
            localStorage.ticken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjMxMjMiLCJyb2xlSWRzIjoiIiwicm9sZXMiOiIiLCJpc3MiOiJxaWFubml1a2VqaSIsImlhdCI6MTQ4Njk1NTA3OSwiZXhwIjoxNTAyNTA3MDc5LCJuYmYiOjE0ODY5NTUwNzl9.dbwjGG4S1o_b9S33h-nmRxrXW8YsLNwwBGmWvteof-A";
            teamLog("register:"+JSON.stringify(data));
            deferred.resolve(data);
		    $ionicLoading.show({template: "注册成功", noBackdrop: true, duration: 1000});
		}).error(function (data, status, headers, config) {
		  	//业务处理
            deferred.reject(data);
		  	$ionicLoading.show({template: "注册失败,请检查网络！", noBackdrop: true, duration: 1000});
		});
        
        return deferred.promise;
	};
	function logout(username){
		localStorage.haslogin = 1;
		$ionicLoading.show({
			template: "退出成功",
			duration:2000
		});
	};
	function islive(){
        var url = 'userinfo.txt';
        $http.get(url)
        .success(function (data) {
            teamLog("islive:"+JSON.stringify(data));
            if(localStorage.haslogin != 0){
	 			$state.go("login");
	 			$ionicLoading.show({template: "登录信息过期，请重新登录", noBackdrop: true, duration: 2000});
	 			return;
            }
	 		if(data['ticken'] != localStorage.ticken ) {
	 			$state.go("login");
	 			$ionicLoading.show({template: "设备更换，请重新登录", noBackdrop: true, duration: 2000});
	 		}
	 		
		}).error(function (data, status, headers, config) {
        	$state.go("login");
		  	$ionicLoading.show({template: "登录失败,请重新登录！", noBackdrop: true, duration: 2000});
		});
	};
	
});
