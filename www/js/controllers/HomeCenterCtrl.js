/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('HomeCenterCtrl', ['$scope',function($scope) {
  	$scope.buttons  = [{
		pageurl: 0,
		name: '设置',
		img: {"background-image": "url(img/me/me01.png)"}
	}, {
		pageurl: 1,
		name: '帮助',
		img: {"background-image": "url(img/me/me02.png)"}
	}, {
		pageurl: 2,
		name: '反馈',
		img: {"background-image": "url(img/me/me03.png)"}
	}, {
		pageurl: 3,
		name: '我发起的',
		img: {"background-image": "url(img/me/me04.png)"}
	}, {
		pageurl: 4,
		name: '我的钱包',
		img: {"background-image": "url(img/me/me05.png)"}
	}, {
		pageurl: 2,
		name: '隐私安全',
		img: {"background-image": "url(img/me/me06.png)"}
	}, {
		pageurl: 3,
		name: '把牵牛派推荐给好友',
		img: {"background-image": "url(img/me/me07.png)"}
	}, {
		pageurl: 4,
		name: '我的消息',
		img: {"background-image": "url(img/me/me08.png)"}
	}, {
		pageurl: 4,
		name: '我的参与',
		img: {"background-image": "url(img/me/me09.png)"}
	}];
}]);

app.controller('HomeqnCtrl', ['$scope',function($scope) {
    $scope.isActive = false;
    $scope.goClick = function(idx){
    	if("0"==idx){
    		$scope.isActive = false;
    	}else{
    		$scope.isActive = true;
    	}
    }
}]);
app.controller('HomehhCtrl', ['$scope',function($scope) {
  
}]);
