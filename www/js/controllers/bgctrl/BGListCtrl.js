/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('BGListCtrl', ['$scope','$state','Utils','BGDataService',function($scope,$state,Utils,BGDataService) {
	
	$scope.gotonext = function(idx){
		var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).addClass("tabs-item-hide");
		$state.go("tab.bg-detail",{listId:idx});
	};
	
	$scope.goPublish = function(idx){
		var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).addClass("tabs-item-hide");
		$state.go("tab.bg-publish",{listId:idx});
	};
	
	$scope.pictures=[{
		style:{"background-color":"red"}
	},{
		style:{"background-color":"blue"}
	},{
		style:{"background-color":"#123"}
	},{
		style:{"background-color":"#000"}
	},{
		style:{"background-color":"red"}
	},{
		style:{"background-color":"#000"}
	},{
		style:{"background-color":"red"}
	},{
		style:{"background-color":"blue"}
	},{
		style:{"background-color":"#123"}
	},{
		style:{"background-color":"#000"}
	}];
	
	$scope.picture2s=[{
		name:"张一成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"张三成功购买了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"李四成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"赵武成功购买了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"牛逼成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"婷婷成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"李静成功购买了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"宋生成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"康康成功发布了一个包裹，售价（3000积分）2017-01-27"
	},{
		name:"黄生成功发布了一个包裹，售价（3000积分）2017-01-27"
	}];
	
	$scope.$on('$ionicView.afterEnter', function() {
        var tabs =document.getElementsByTagName('ion-tabs');
        angular.element(tabs).removeClass("tabs-item-hide");
      
    });
}]);
