/**
 * Created by kang on 2016/12/19.
 */
'use strict';

app.controller('LoginCtrl', ['$scope','$state','LoginService',function($scope,$state,LoginService) {
   	var tabsbutton = [{
   		title:"登录",
   		btnsubmit:"login",
   		regshow:{"display":"none"},
   		logstyle:{
   			"margin-top":"1px",
		    "border-style":"solid",
		    "color":"#fd8003"
   		}
   		
   	},{
   		title:"注册",
   		btnsubmit:"registe",
   		logshow:{"display":"none"},
   		regstyle:{
   			"margin-top":"1px",
		    "border-style":"solid",
		    "color":"#fd8003"
   		}
   	}];
   	teamLog('loginCtrl');
   	$scope.tab = tabsbutton[0];
   	$scope.selecttabs = function(idx){
   		$scope.tab = tabsbutton[idx];
   	}
   	
   	$scope.user = {
   		"username":"123123",
   		"password":"123123",
   		"repassword":"123123",
   		"isaccept":true
   	};
   	
   	$scope.goSubmit = function(idx){
   		teamLog(idx);
   		//teamLog($scope.user.username);
   		var promise = null;
   		if('0'==idx){
   			promise = LoginService.logon($scope.user.username,$scope.user.password);
   		}else{
   			promise = LoginService.register("123123","123123");
   		}
		promise.then(function(data){
            //teamLog("goSubmit:"+JSON.stringify(data));
            $state.go("tab.home");
        },function(data){
        	teamLog(data);
        });
   	}
   	
}]);